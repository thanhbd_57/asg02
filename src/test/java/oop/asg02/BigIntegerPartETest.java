package oop.asg02;

import org.junit.*;
import static org.junit.Assert.*;

public class BigIntegerPartETest {
    @Test
    public void testClone()
    {
        BigInteger bigInt = new BigInteger(10);
        assertEquals("10", bigInt.clone().toString());
    }

    @Test
    public void testcompareToLower()
    {
        BigInteger bigInt1 = new BigInteger(10);
        BigInteger bigInt2 = new BigInteger(9);
        assertEquals(1, bigInt1.compareTo(bigInt2));
    }

    @Test
    public void testcompareToEqual()
    {
        BigInteger bigInt1 = new BigInteger(10);
        BigInteger bigInt2 = new BigInteger(10);
        assertEquals(0, bigInt1.compareTo(bigInt2));
    }

    @Test
    public void testcompareToBigger()
    {
        BigInteger bigInt1 = new BigInteger(10);
        BigInteger bigInt2 = new BigInteger(11);
        assertEquals(-1, bigInt1.compareTo(bigInt2));
    }
}
