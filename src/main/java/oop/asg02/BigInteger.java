package oop.asg02;

public class BigInteger {
	private int[] num =  new int[1000];
	private int len=0;
	int carry=0;
	
	public BigInteger(long init) {
		int i=0;
        long m=init;
        len=0;
		if (m==0) {len=1;num[0]=0;}
		while (m!=0){
			len++;
			num[i]=(int)m%10;
			m = (m-m%10)/10;
			i++;
		}
	}

    public BigInteger(String init){
        int i=0;
        while(init.charAt(0)=='0'&& init.length()>1){
            init= init.substring(1);
        }
        len = 0;
        for(i=init.length()-1;i>=0;--i){
            num[len++] = Character.digit(init.charAt(i), 10);
        }
    }

	public String toString() {
		int i=0,j=0;
		String a =new String();
        a= "";
		for(i=len-1;i>=0;i--){
			a +=num[i];
			j++;
		}
		return (a);
	}

    public long toLong(){
        int i=0;
        long a=0,s=1;
        for(i=0;i<len;i++){
            a+=num[i]*s;
            s*=10;
        }
        return a;
    }

	public boolean equals(Object other) {
        return toString().equals(other.toString());
    }

    public int add1 (int a, int b){
        int i;
        i=a+b+carry;
        carry=i/10;
        return (i%10);
    }

    public int sub1 (int a, int b){
        int i;
        i=a-b-carry+10;
        carry=1-i/10;
        return (i%10);
    }

    public BigInteger subtract(BigInteger BigInt){
        BigInteger s= new BigInteger(0);
        int i=0,m=0;
        carry=0;
        if (len>BigInt.len)  m=BigInt.len; else m=len;
        while (i<=m-1) {s.num[i]=sub1(num[i],BigInt.num[i]);i++;}
        while (i<=len-1) {s.num[i] = sub1(num[i],0);i++;}
        i--;
        while (i>0&&s.num[i]==0){i--;};
        s.len=++i;
        return s;
    }

    public BigInteger add(BigInteger BigInt){
        int i=0,m=0;
        BigInteger s= new BigInteger(0);
        carry=0;
        if (len>BigInt.len)  m=BigInt.len; else m=len;
        while (i<=m-1) {s.num[i]=add1(num[i],BigInt.num[i]);i++;}
        if(len>BigInt.len)
            while (i<=len-1) {s.num[i] = add1(num[i],0);i++;}
        else
            while (i<=BigInt.len-1) {s.num[i] = add1(BigInt.num[i],0);i++;}
        if (carry!=0) {s.num[i] = carry;i++;}
        s.len=i;
        return s;
    }

    public int compareTo(BigInteger other){
        int i;
        if (other.len>len) return -1;
        if (len>other.len) return 1;
        for (i=len;i>=0;i--){
            if (other.num[i]>num[i]) return -1;
            if (num[i]>other.num[i]) return 1;
        }
        return 0;
    }

    public BigInteger clone(){
        BigInteger s= new BigInteger(this.toString());
        return  s;
    }
}

